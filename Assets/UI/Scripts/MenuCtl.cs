﻿// ------------------------ //
//  "Dont Paint Me" - 2017  //
//    Daniel Camba Lamas    //
//  <cambalamas@gmail.com>  //
// ------------------------ //

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuCtl : MonoBehaviour {

	// ************************************************************************
	//	--- VARIABLES ---
	// ************************************************************************

	// From editor
	public GameObject MainMenu;
	public GameObject MainMenuButtonPLAY;
	public GameObject MainMenuButtonOPTS;
	public GameObject MainMenuButtonQUIT;
	public GameObject HighScoreRED;
	public GameObject HighScoreBLUE;
	public GameObject WinsRED;
	public GameObject WinsBLUE;
	public GameObject OptionMenu;
	public GameObject OptionMenuButtonBACK;
	public GameObject OptionMenuAxis;
	public GameObject OptionMenuDifficult;

	// Backend
	bool goingUp;
	float curLim;
	float limUp = 80f;
	float limDown = 35f;

	// ************************************************************************
	//	--- UNITY BASE METHODS ---
	// ************************************************************************

	void Awake() {
		// Preserve persistency
		var yAxis = (PlayerPrefs.GetInt("Y-Axis") == 1) ? true : false;
		OptionMenuAxis.GetComponent<Toggle>().isOn = yAxis;
		OptionMenuDifficult.GetComponent<Dropdown>().value = PlayerPrefs.GetInt("Difficulty");
	}

	void Start() {
		curLim = limUp;
		MainMenu.SetActive(true);
		OptionMenu.SetActive(false);
		UpdateScores();
		MainMenuButtonPLAY.GetComponent<Button>().Select();
	}

	void FixedUpdate() {
		if (curLim > limDown && !goingUp) {
			curLim -= 0.1f;
			Camera.main.fieldOfView = curLim;
		} else {
			goingUp = true;
			curLim += 0.1f;
			Camera.main.fieldOfView = curLim;
		}
		if (curLim > limUp) { goingUp = false; }

	}

	// ************************************************************************
	//	--- HELPERS ---
	// ************************************************************************

	// Main menu

	public void Play() {
		SceneManager.LoadScene(1);
	}

	public void Options() {
		MainMenu.SetActive(false);
		OptionMenu.SetActive(true);
		OptionMenuButtonBACK.GetComponent<Button>().Select();
	}

	public void Quit() {
		print("QUIT");
		Application.Quit();
	}

	public void ClickOnSign() {
		Application.OpenURL("https://www.linkedin.com/in/daniel-camba-lamas-76374198/");
	}

	public void UpdateScores() {
		var hsRed = Mathf.Clamp(PlayerPrefs.GetInt("HighScoreRED"), 0, 9999);
		HighScoreRED.GetComponent<Text>().text = hsRed.ToString();
		var hsBlue = Mathf.Clamp(PlayerPrefs.GetInt("HighScoreBLUE"), 0, 9999);
		HighScoreBLUE.GetComponent<Text>().text = hsBlue.ToString();
		var winsRed = Mathf.Clamp(PlayerPrefs.GetInt("WinsRED"), 0, 9999);
		WinsRED.GetComponent<Text>().text = winsRed.ToString();
		var winsBlue = Mathf.Clamp(PlayerPrefs.GetInt("WinsBLUE"), 0, 9999);
		WinsBLUE.GetComponent<Text>().text = winsBlue.ToString();
	}

	// Options menu

	public void SwitchYAxis(bool state) {
		var i = (state) ? 1 : 0;
		PlayerPrefs.SetInt("Y-Axis", i);
		PlayerPrefs.Save();
	}

	public void SwitchDifficulty(int index) {
		PlayerPrefs.SetInt("Difficulty", index);
		PlayerPrefs.Save();
	}

	public void BackToMainMenu() {
		OptionMenu.SetActive(false);
		MainMenu.SetActive(true);
		MainMenuButtonPLAY.GetComponent<Button>().Select();
	}

	public void ResetScores() {
		PlayerPrefs.SetInt("HighScoreRED", 0);
		PlayerPrefs.SetInt("HighScoreBLUE", 0);
		PlayerPrefs.SetInt("WinsRED", 0);
		PlayerPrefs.SetInt("WinsBLUE", 0);
		UpdateScores();
		BackToMainMenu();
	}

}
