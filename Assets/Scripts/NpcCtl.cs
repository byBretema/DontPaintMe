﻿// ------------------------ //
//  "Dont Paint Me" - 2017  //
//    Daniel Camba Lamas    //
//  <cambalamas@gmail.com>  //
// ------------------------ //

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NpcCtl : MonoBehaviour {

	// ************************************************************************
	//	--- VARIABLES ---
	// ************************************************************************

	// From editor
	public Transform bornPaticle;
	public Transform deathParticle;
	public Texture2D tex_neutral;
	public List<Texture2D> texs_red;
	public List<Texture2D> texs_blue;

	// Serializable
	int hits;
	int neededHits;
	float alertDist;
	float animBaseSpeed;
	float agentBaseSpeed;
	float animAlertSpeed;
	float agentAlertSpeed;

	// Backend
	float X;
	float Z;
	Vector3 tP;
	Vector3 tE;
	string activeColor;

	// Unity objects
	Renderer body;
	Renderer bodyMap;
	Animator animCtl;
	NavMeshAgent agent;
	GameObject gamectl;
	ParticleSystem.MainModule bornPsMain;

	// ************************************************************************
	//	--- UNITY BASE METHODS ---
	// ************************************************************************

	void Awake() {
		int dif = PlayerPrefs.GetInt("Difficulty");
		switch (dif) {
			case 1:
				InitVars(0, 3, 6f, 2.3f, 5f, 3f, 300f);
				break;
			case 2:
				InitVars(0, 3, 8f, 2.5f, 5.5f, 3.3f, 500f);
				break;
			default:
				InitVars(0, 3, 4f, 2.3f, 4.5f, 2.8f, 150f);
				break;
		}
	}

	void Start() {
		bornPsMain = bornPaticle.GetComponent<ParticleSystem>().main;
		bornPsMain.startColor = Color.white;
		AudioManager.instance.Play("NpcBorn");
		Instantiate(bornPaticle, transform.position, Quaternion.Euler(-90f, 0f, 0f));
		// Vars init
		X = (GameObject.FindWithTag("Terrain").transform.lossyScale.x - 25) * 0.5f;
		Z = (GameObject.FindWithTag("Terrain").transform.lossyScale.z - 25) * 0.5f;
		// Agent init setup
		agent = GetComponent<NavMeshAgent>();
		agent.autoBraking = false;
		agent.autoRepath = true;
		agent.destination = GetRandPosition();
		// Texture init setup
		body = transform.GetChild(transform.childCount - 2).GetComponent<Renderer>();
		bodyMap = body;
		body.material.mainTexture = tex_neutral;
		bodyMap.material.mainTexture = tex_neutral;
		// Animation setup
		animCtl = GetComponent<Animator>();
		animCtl.Play("walk");
		// Objects setup
		gamectl = GameObject.FindWithTag("GameController").gameObject; // for update stats points
	}

	void FixedUpdate() {
		tE = GameObject.FindWithTag("Enemy").transform.position;
		tP = GameObject.FindWithTag("Player").transform.position;
		// Re-target direction
		if (!agent.pathPending && agent.remainingDistance < 0.75f)
			agent.destination = GetRandPosition();
		// Check distance to the player or enemy if it's low, run!
		if (Alert(transform.position)) {
			animCtl.speed = animAlertSpeed;
			agent.speed = agentAlertSpeed;
		}
		if (!Alert(transform.position)) {
			animCtl.speed = animBaseSpeed;
			agent.speed = agentBaseSpeed;
		}
	}

	// ************************************************************************
	//	--- HELPERS ---
	// ************************************************************************

	// Response to one hit
	void Hitted(string who) {

		if (who == "red") {
			bornPsMain.startColor = Color.red;
			Instantiate(bornPaticle, transform.position, Quaternion.Euler(-90f, 0f, 0f));
		}
		if (who == "blue") {
			bornPsMain.startColor = Color.blue;
			Instantiate(bornPaticle, transform.position, Quaternion.Euler(-90f, 0f, 0f));
		}

		if (hits == 0) { activeColor = who; }

		if (who == activeColor) {
			hits++;
			if (who == "red") {
				AudioManager.instance.Play("RedImpact");
				Hit("red", texs_red);
			}
			if (who == "blue") {
				AudioManager.instance.Play("BlueImpact");
				Hit("blue", texs_blue);
			}
			if (hits >= 3) {
				if (who == "red")
					AudioManager.instance.Play("NpcDeadByRed");
				if (who == "red")
					AudioManager.instance.Play("NpcDeadByBlue");
				Instantiate(deathParticle, transform.position, Quaternion.identity);
				Destroy(gameObject, 0.3f);
			}
		} else {
			hits--;
			if (activeColor == "red") Hit("red", texs_red);
			if (activeColor == "blue") Hit("blue", texs_blue);
		}
	}

	// Generate a random position to change direction of NPC
	Vector3 GetRandPosition() {
		Vector3 rPos = Vector3.zero;
		for (int i = 0; i <= 5; i++) {
			rPos = new Vector3(Random.Range(-X, X), 0f, Random.Range(-Z, Z));
			if (!Alert(rPos)) { break; }
		}
		return rPos;
	}

	// Response to hit.
	void Hit(string who, List<Texture2D> texs) {
		if (hits == neededHits) {
			gamectl.SendMessage("AddPointsTo", who);
		} else if (hits <= neededHits) {
			body.material.mainTexture = texs[hits];
			bodyMap.material.mainTexture = texs[hits];
		}
	}

	// True if this NPC is near to the Player or IA, false otherwise.
	bool Alert(Vector3 t) {
		return ((tE - t).magnitude <= alertDist) || ((tP - t).magnitude <= alertDist);
	}

	// Variables initializer
	void InitVars(int hits, int neededHits, float alertDist, float animBaseSpeed, float agentBaseSpeed, float animAlertSpeed, float agentAlertSpeed) {
		this.hits = hits;
		this.neededHits = neededHits;
		this.alertDist = alertDist;
		this.animBaseSpeed = animBaseSpeed;
		this.agentBaseSpeed = agentBaseSpeed;
		this.animAlertSpeed = animAlertSpeed;
		this.agentAlertSpeed = agentAlertSpeed;
	}

}
