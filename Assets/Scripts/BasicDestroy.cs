﻿// ------------------------ //
//  "Dont Paint Me" - 2017  //
//    Daniel Camba Lamas    //
//  <cambalamas@gmail.com>  //
// ------------------------ //

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicDestroy : MonoBehaviour {

	// ************************************************************************
	//	--- UNITY BASE METHODS ---
	// ************************************************************************

	void Start() { Destroy(gameObject, 0.6f); }
}
