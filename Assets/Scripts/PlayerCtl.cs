﻿// ------------------------ //
//  "Dont Paint Me" - 2017  //
//    Daniel Camba Lamas    //
//  <cambalamas@gmail.com>  //
// ------------------------ //

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements.StyleEnums;
using UnityEngine.Timeline;

public class PlayerCtl : MonoBehaviour {

	// ************************************************************************
	//	--- VARIABLES ---
	// ************************************************************************

	// From editor
	public GameObject brush;

	// Serializable
	float animWalkSpeed;
	float agentWalkSpeed;
	float animRunSpeed;
	float agentRunSpeed;
	float throwPauseTime;

	// Backend
	float speed;
	float timeMoving;
	Animator animCtl;
	bool brushThrown = true;
	CharacterController charCtl;

	// ************************************************************************
	//	--- UNITY BASE METHODS ---
	// ************************************************************************

	void Awake() {
		int dif = PlayerPrefs.GetInt("Difficulty");
		switch (dif) {
			case 1:
				InitVars(2.5f, 6f, 3f, 9f, 0.2f);
				break;
			case 2:
				InitVars(2.3f, 5.5f, 2.8f, 8.5f, 0.3f);
				break;
			default:
				InitVars(2.8f, 8f, 3.4f, 11f, 0.1f);
				break;
		}
	}

	void Start() {
		animCtl = GetComponent<Animator>();
		charCtl = GetComponent<CharacterController>();
	}

	void FixedUpdate() {

		var newPos = GetPlayerMovement();

		// THERE IS MOVEMENT
		if (newPos.magnitude > 0f) {
			timeMoving += 0.1f;
			animCtl.Play("walk");
			// Walk animation
			if (timeMoving < 5f) {
				speed = agentWalkSpeed * Time.deltaTime;
				animCtl.speed = animWalkSpeed;
			}
			// Run animation
			else {
				speed = agentRunSpeed * Time.deltaTime;
				animCtl.speed = animRunSpeed;
			}
			// Apply:  Movement
			charCtl.Move(newPos * speed);
			transform.position = new Vector3(transform.position.x, 0f, transform.position.z); // Avoid Y variations
			// Apply: Rotation
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(newPos * speed), 0.3f);
		}
		// THERE ISN'T MOVEMENT
		else {
			timeMoving = 0f;
			animCtl.speed = 1;
			animCtl.Play("idle");
		}
	}

	void Update() {
		// Try to paint a NPC
		if (Input.GetButtonDown("Fire1")) {
			if (brushThrown) {
				StartCoroutine("ThrowBrush");
				brushThrown = !brushThrown;
			}
		}
	}

	// ************************************************************************
	//	--- HELPERS ---
	// ************************************************************************

	// Throw the paint sphere
	IEnumerator ThrowBrush() {
		var go = Instantiate(brush, transform) as GameObject;
		go.transform.parent = transform.parent;
		yield return new WaitForSeconds(throwPauseTime);
		brushThrown = true;
	}

	// Ignore the Y position from a given Vec3
	Vector3 HumanVec3(Vector3 dir) {
		return new Vector3(dir.x, transform.position.y, dir.z).normalized;
	}

	// Compute player movement based on camera axis
	Vector3 GetPlayerMovement() {
		var cam = Camera.main.transform;

		var V = Input.GetAxis("Vertical");
		var H = Input.GetAxis("Horizontal");

		bool up = V > 0;
		bool right = H > 0;
		bool down = V < 0;
		bool left = H < 0;

		Vector3 newPos = new Vector3(0f, 0f, 0f).normalized;
		if (up) newPos = HumanVec3(cam.forward);
		if (down) newPos = HumanVec3(-cam.forward);
		if (right) newPos = HumanVec3(cam.right);
		if (left) newPos = HumanVec3(-cam.right);
		if (up && right) newPos = HumanVec3(cam.forward + cam.right);
		if (up && left) newPos = HumanVec3(cam.forward + -cam.right);
		if (down && right) newPos = HumanVec3(-cam.forward + cam.right);
		if (down && left) newPos = HumanVec3(-cam.forward + -cam.right);

		return newPos.normalized;
	}

	// Variables initializer
	void InitVars(float animWalkSpeed, float agentWalkSpeed, float animRunSpeed, float agentRunSpeed, float throwPauseTime) {
		this.animWalkSpeed = animWalkSpeed;
		this.agentWalkSpeed = agentWalkSpeed;
		this.animRunSpeed = animRunSpeed;
		this.agentRunSpeed = agentRunSpeed;
		this.throwPauseTime = throwPauseTime;
	}

}
